package de.febanhd.friendsystem.commands;

import java.sql.SQLException;
import java.util.UUID;

import de.febanhd.friendsystem.FriendSystem;
import de.febanhd.friendsystem.utils.NameFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class FriendCommand extends Command {
	
	private final String FRIEND_HELP = "�e/friend add <Spieler> �7F�ge einen neuen Freund hinzu."
										+ "\n�e/friend accept <Spieler> �7Nehme eine Freundschaftsanfrage an."
										+ "\n�e/friend remove <Spieler> �7Entferne einen Freund."
										+ "\n�e/friend jump <Spieler> �7Springe einem Freund nach."
										+ "\n�e/msg <Spieler> �7Schreibe eine Private Nachricht."
										+ "\n�e/r �7Schreibe dem Freund, dem du zuletzt geschrieben hast.";

	public FriendCommand(String name) {
		super(name);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender instanceof ProxiedPlayer) {
			ProxiedPlayer player = (ProxiedPlayer) sender;
			if(args.length == 2) {
				if(args[0].equalsIgnoreCase("add")) {
					ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[1]);
					if(target != null && target.isConnected()) {
						try {
							FriendSystem.friendManager.requestPlayer(player, target);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else {
						player.sendMessage(FriendSystem.PREFIX + "�c" + args[1] + " ist nicht Online!");
					}
				}else if(args[0].equalsIgnoreCase("accept")) {
					try {
						FriendSystem.friendManager.confirmRequest(player, args[1]);
					} catch (Exception e) {
						// TODO: handle exception
					}
				}else if(args[0].equalsIgnoreCase("deny")) {
					FriendSystem.friendManager.denyRequest(player, args[1]);
				}else if(args[0].equalsIgnoreCase("jump")) {
					ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[1]);
					if(target == null || !target.isConnected())
						player.sendMessage(FriendSystem.PREFIX + "�c" + args[1] + " ist nicht Online!");
					else
						FriendSystem.friendManager.jump(player, target);
				}else if(args[0].equalsIgnoreCase("remove")) {
					if(NameFetcher.getUUID(args[1]) != null) {
						try {
							FriendSystem.friendManager.removeFriend(player.getUniqueId(), UUID.fromString(NameFetcher.getUUID(args[1])));
						} catch (SQLException e) {e.printStackTrace();}
					}else
						player.sendMessage(FriendSystem.PREFIX + "�cDu bist nicht mit �4" + args[1] + " �cbefreundet.");
				}else
					player.sendMessage(FRIEND_HELP);
			}else if(args.length == 1) {
				if(args[0].equalsIgnoreCase("list")) {
					player.sendMessage("�eFreunde:");
					if(FriendSystem.friendManager.getFriends(player.getUniqueId()) == null)
						player.sendMessage("�8- �cKeine");
					else {
						for(String current : FriendSystem.friendManager.getFriends(player.getUniqueId())) {	
							if(current == null)  current = "�cKeine";
								player.sendMessage("�8- �7" + current);
						}
					}
					player.sendMessage("�eAnfragen:");
					if(FriendSystem.friendManager.getRequests(player.getUniqueId()) == null) {
						player.sendMessage("�8- �cKeine");
					}else {
						for(String current : FriendSystem.friendManager.getRequests(player.getUniqueId())) {
							if(current == null) current = "�cKeine";
 								player.sendMessage("�8- �7" + current);
							
						}
					}
				}
			}else
				player.sendMessage(FRIEND_HELP);
			
		}
		
	}
	
	// /friend add FebanHD
//		0		1	2
}
