package de.febanhd.friendsystem.commands;

import de.febanhd.friendsystem.FriendSystem;
import de.febanhd.friendsystem.api.FriendManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ReCommand extends Command {

	public ReCommand(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender instanceof ProxiedPlayer) {
			ProxiedPlayer player = (ProxiedPlayer) sender;
			if(args.length > 0) {
				if(!FriendSystem.friendManager.lastMessage.containsKey(player.getUniqueId())) {
					player.sendMessage(FriendSystem.PREFIX + "�cDu hast keinem Spieler zuvor geschrieben.");
					return;
				}
				ProxiedPlayer target = ProxyServer.getInstance().getPlayer(FriendSystem.friendManager.lastMessage.get(player.getUniqueId()));
				if(target != null && target.isConnected()) {
					FriendManager manager = FriendSystem.friendManager;
					String msg = "";
					for (int i = 0; i != args.length; i++)
						msg += args[i] + " ";
					manager.message(player, target, msg);
				}else
					player.sendMessage(FriendSystem.PREFIX + "�cDu bist nicht mit �4" + target.getName() + " �cbefreundet.");
			}else
				player.sendMessage(FriendSystem.PREFIX + "�7Benutze: �e/r <Nachrich>");
		}
	}

}
