package de.febanhd.friendsystem.listener;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

import de.febanhd.friendsystem.FriendSystem;
import net.Lenni0451.EasySQL.Table;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerConnectionListener implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onJoin(PostLoginEvent event) {
		Table onlineTable = FriendSystem.getInstance().getDatabase().getTable("players");
		Table table = FriendSystem.getInstance().getDatabase().getTable("friend");
		ProxiedPlayer player = event.getPlayer();
		try {
			if(!FriendSystem.friendManager.isRegistered(player.getUniqueId())) {
				table.addRow("UUID", player.getUniqueId().toString());
				onlineTable.addRow("UUID", player.getUniqueId().toString());
			}
			table.getRowsWhere("UUID", player.getUniqueId().toString()).get(0).updateColumn("NAME", player.getName());
			onlineTable.getRowsWhere("UUID", player.getUniqueId().toString()).get(0).updateColumn("ONLINE", "true");
		} catch (Exception e) {
			e.printStackTrace();
		}
		for(String friendNames : FriendSystem.friendManager.getFriends(player.getUniqueId())) {
			ProxiedPlayer friend = ProxyServer.getInstance().getPlayer(friendNames);
			if(friend != null && friend.isConnected()) {
				friend.sendMessage(FriendSystem.PREFIX + "�e" + player.getDisplayName() + " �7ist nun �aOnline");
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onQuit(PlayerDisconnectEvent event) {
		ProxiedPlayer player = event.getPlayer();
		try {
			FriendSystem.getInstance().getDatabase().getTable("players").getRowsWhere("UUID", event.getPlayer().getUniqueId().toString()).get(0).updateColumn("ONLINE", "false");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		HashMap<UUID, UUID> lm = FriendSystem.friendManager.lastMessage;
		if(lm.containsKey(player.getUniqueId())) {
			lm.remove(player.getUniqueId());
			ProxyServer.getInstance().getPlayers().forEach(all -> {
				if(all != player) {
					if(lm.get(all.getUniqueId()).equals(player.getUniqueId()))
						lm.remove(all.getUniqueId());
				}
			});
		}
		for(String friendNames : FriendSystem.friendManager.getFriends(player.getUniqueId())) {
			ProxiedPlayer friend = ProxyServer.getInstance().getPlayer(friendNames);
			if(friend != null && friend.isConnected()) {
				friend.sendMessage(FriendSystem.PREFIX + "�e" + player.getDisplayName() + " �7ist nun �cOffline");
			}
		}
		
	}

}
