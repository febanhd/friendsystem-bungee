package de.febanhd.friendsystem.api;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import de.febanhd.friendsystem.FriendSystem;
import de.febanhd.friendsystem.utils.NameFetcher;
import net.Lenni0451.EasySQL.Database;
import net.Lenni0451.EasySQL.Row;
import net.Lenni0451.EasySQL.Table;
import net.Lenni0451.EasySQL.info.rows.RowInfo;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class FriendManager {

	private Database database;
	public HashMap<UUID, UUID> lastMessage = new HashMap<UUID, UUID>();  

	public FriendManager() {
		database = FriendSystem.getInstance().getDatabase();
	}

	@SuppressWarnings("deprecation")
	public void requestPlayer(ProxiedPlayer player, ProxiedPlayer target) throws SQLException {
		if (player.getUniqueId().equals(target.getUniqueId())) {
			player.sendMessage(FriendSystem.PREFIX + "�cDu kannst dir nicht selbst eine Anfrage schicken!");
			return;
		}
		if (hasRequest(target.getUniqueId(), player.getName())) {
			player.sendMessage(FriendSystem.PREFIX + "�cDu hast diesem Spieler bereits eine Anfrage geschickt!");
			return;
		}
		if (isFriend(target.getUniqueId(), player.getUniqueId())) {
			player.sendMessage(FriendSystem.PREFIX + "�cDu bist bereits mit diesem Spieler befreundet!");
			return;
		}
		if (getFriendSize(target.getUniqueId()) > 200) {
			player.sendMessage(FriendSystem.PREFIX
					+ "�cDu kannst diesem Spieler keine Freundschaftsanfragen schicken, da er/sie das Maximale limit an Freunden erreicht hat!");
			return;
		}
		if (getRequestSize(target.getUniqueId()) > 200) {
			player.sendMessage(FriendSystem.PREFIX
					+ "�cDu kannst diesem Spieler keine Freundschaftsanfragen schicken, da er/sie das Maximale limit an Anfragen erreicht hat!");
			return;
		}
		if (getFriendSize(player.getUniqueId()) > 200) {
			player.sendMessage(FriendSystem.PREFIX
					+ "�cDu kannst diesem Spieler keine Freundschaftsanfragen schicken, da du das Maximale limit an Freunden erreicht hat!");
			return;
		}
		if (getRequestSize(player.getUniqueId()) > 200) {
			player.sendMessage(FriendSystem.PREFIX
					+ "�cDu kannst diesem Spieler keine Freundschaftsanfragen schicken, da du das Maximale limit an Anfragen erreicht hat!");
			return;
		}
		Table friendTable = database.getTable("friend");
		List<Row> rows = friendTable.getRowsWhere(new RowInfo("UUID", target.getUniqueId().toString()));
		Row row = rows.get(0);
		String requests = (String) row.getColumn("REQUESTS");
		if (requests == null || requests.equalsIgnoreCase("null")) {
			requests = player.getUniqueId().toString() + ";";
		} else {
			requests = requests + player.getUniqueId() + ";";
		}
		friendTable.getRowsWhere("UUID", target.getUniqueId().toString()).get(0).updateColumn("REQUESTS", requests);
		TextComponent textComponentAccept = new TextComponent("�a/friend accept");
		textComponentAccept
				.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend accept " + player.getName()));
		textComponentAccept.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[0]));
		TextComponent textComponentDeny = new TextComponent("�c/friend deny");
		textComponentDeny
				.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend deny " + player.getName()));
		player.sendMessage(FriendSystem.PREFIX + "�aDu hast �e" + target.getDisplayName()
				+ " �aeine Freundschaftsanfrage geschickt.");
		target.sendMessage(FriendSystem.PREFIX + "�eDu hast eine Freundschaftsanfrage von �a" + player.getDisplayName()
				+ " �eerhalten.");
		target.sendMessage(textComponentAccept);
		target.sendMessage(textComponentDeny);
	}

	@SuppressWarnings("deprecation")
	public void confirmRequest(ProxiedPlayer player, String targetName) {
		try {
			if (hasRequest(player.getUniqueId(), targetName)) {
				Table friendTable = database.getTable("friend");
				List<Row> rows = friendTable.getRowsWhere(new RowInfo("UUID", player.getUniqueId().toString()));
				Row row = rows.get(0);
				String requests = (String) row.getColumn("REQUESTS");
				requests = requests.replaceAll(NameFetcher.getUUID(targetName) + ";", "");
				friendTable.getRowsWhere("UUID", player.getUniqueId().toString()).get(0).updateColumn("REQUESTS",
						requests);
			} else {
				player.sendMessage(FriendSystem.PREFIX + "�cDu hast keine Anfrage von diesem Spieler!");
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			Table friendTable = database.getTable("friend");
			List<Row> rows = friendTable.getRowsWhere(new RowInfo("UUID", player.getUniqueId().toString()));
			Row row = rows.get(0);
			String friends = (String) row.getColumn("FRIENDS");
			if (friends == null || friends.equalsIgnoreCase("null")) {
				friends = NameFetcher.getUUID(targetName) + ";";
			} else {
				friends = friends + NameFetcher.getUUID(targetName) + ";";
			}
			friendTable.getRowsWhere("UUID", player.getUniqueId().toString()).get(0).updateColumn("FRIENDS", friends);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			Table friendTable = database.getTable("friend");
			List<Row> rows = friendTable.getRowsWhere(new RowInfo("UUID", NameFetcher.getUUID(targetName)));
			Row row = rows.get(0);
			String friends = (String) row.getColumn("FRIENDS");
			if (friends == null || friends.equalsIgnoreCase("null")) {
				friends = player.getUniqueId().toString() + ";";
			} else {
				friends = friends + player.getUniqueId().toString() + ";";
			}
			friendTable.getRowsWhere("UUID", NameFetcher.getUUID(targetName)).get(0).updateColumn("FRIENDS", friends);
		} catch (Exception e) {
			e.printStackTrace();
		}
		player.sendMessage(FriendSystem.PREFIX + "�aDu hast die Anfrage von �e" + targetName + " �aangenommen.");
		ProxiedPlayer targetPlayer = ProxyServer.getInstance().getPlayer(targetName);
		if (targetPlayer != null && targetPlayer.isConnected())
			targetPlayer.sendMessage(FriendSystem.PREFIX + "�a" + player.getDisplayName()
					+ " hat deine �eFreundschaftsanfrage �aangenommen.");
	}
	
	@SuppressWarnings("deprecation")
	public void removeFriend(UUID uuid, UUID targetUUID) throws SQLException {
		Table friendTable = database.getTable("friend");
		if(isFriend(uuid, targetUUID)) {
			//Remove targetsUUID of playersFriends
			String friends = (String) friendTable.getRowsWhere("UUID", uuid.toString()).get(0).getColumn("FRIENDS");
			friends = friends.replaceAll(targetUUID.toString() + ";", "");
			friendTable.getRowsWhere("UUID", uuid.toString()).get(0).updateColumn("FRIENDS", friends);
			//Remove playersUUID of tragetFriends
			String friendsTarget = (String) friendTable.getRowsWhere("UUID", targetUUID.toString()).get(0).getColumn("FRIENDS");
			friendsTarget = friendsTarget.replaceAll(uuid.toString() + ";", "");
			friendTable.getRowsWhere("UUID", targetUUID.toString()).get(0).updateColumn("FRIENDS", friendsTarget);
			ProxyServer.getInstance().getPlayer(uuid).sendMessage(FriendSystem.PREFIX + "�7Du bist nun nicht mehr mit �e" + NameFetcher.getName(targetUUID.toString()) + " �7befreundet.");
		}else
			ProxyServer.getInstance().getPlayer(uuid).sendMessage(FriendSystem.PREFIX + "�cDu bist nicht mit �4" + NameFetcher.getName(targetUUID.toString()) + " �cbefreundet.");
	}

	@SuppressWarnings("deprecation")
	public void denyRequest(ProxiedPlayer player, String targetName) {
		try {
			if (hasRequest(player.getUniqueId(), targetName)) {
				Table friendTable = database.getTable("friend");
				List<Row> rows = friendTable.getRowsWhere(new RowInfo("UUID", player.getUniqueId().toString()));
				Row row = rows.get(0);
				String requests = (String) row.getColumn("REQUESTS");
				requests = requests.replaceAll(NameFetcher.getUUID(targetName) + ";", "");
				friendTable.getRowsWhere("UUID", player.getUniqueId().toString()).get(0).updateColumn("REQUESTS",
						requests);
				player.sendMessage(
						FriendSystem.PREFIX + "�7Du hast die Anfrage von �c" + targetName + " �cabgelehnt�7.");
				ProxiedPlayer targetPlayer = ProxyServer.getInstance().getPlayer(targetName);
				if (targetPlayer != null && targetPlayer.isConnected())
					targetPlayer.sendMessage(FriendSystem.PREFIX + "�c" + player.getDisplayName()
							+ " hat deine �eFreundschaftsanfrage �cabgelenht!");
			} else {
				player.sendMessage(FriendSystem.PREFIX + "�cDu hast keine Anfrage von diesem Spieler!");
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<String> getFriends(UUID uuid) {
		List<String> friends = new ArrayList<>();
		try {
			Table friendTable = database.getTable("friend");
			List<Row> rows = friendTable.getRowsWhere(new RowInfo("UUID", uuid.toString()));
			Row row = rows.get(0);
			String friendUUIDS = (String) row.getColumn("FRIENDS");
			if (friendUUIDS == null || friendUUIDS == "")
				return null;
			String[] args = friendUUIDS.split(";");
			for (int i = 0; i < args.length; i++) {
				String name = NameFetcher.getName(args[i]);
				friends.add(name);
			}
		} catch (SQLException e) {
			// TODO: handle exception
		}
		return friends;
	}

	public List<String> getRequests(UUID uuid) {
		List<String> friends = new ArrayList<>();
		try {
			Table friendTable = database.getTable("friend");
			List<Row> rows = friendTable.getRowsWhere(new RowInfo("UUID", uuid.toString()));
			Row row = rows.get(0);
			String friendUUIDS = (String) row.getColumn("REQUESTS");
			if (friendUUIDS == null || friendUUIDS == "")
				return null;
			String[] args = friendUUIDS.split(";");
			for (int i = 0; i < args.length; i++) {
				friends.add(NameFetcher.getName(args[i]));
			}
			return friends;
		} catch (SQLException e) {
			return null;
		}
	}

	public boolean hasRequest(UUID playerUUID, String requestName) {
		if (getRequests(playerUUID) == null)
			return false;
		for (String rName : getRequests(playerUUID)) {
			if (requestName.equalsIgnoreCase(rName)) {
				return true;
			}
		}
		return false;
	}

	public boolean isOnline(String playerName) {
		try {
			if (ProxyServer.getInstance().getPlayer(playerName).isConnected())
				;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public int getFriendSize(UUID uuid) {
		try {
			Table friendTable = database.getTable("friend");
			List<Row> rows = friendTable.getRowsWhere(new RowInfo("UUID", uuid.toString()));
			Row row = rows.get(0);
			String friends = (String) row.getColumn("FRIENDS");
			if (friends == null || friends.equalsIgnoreCase("null"))
				return 0;
			String[] size = friends.split(";");
			return size.length;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return 0;
	}

	public int getRequestSize(UUID uuid) {
		try {
			Table friendTable = database.getTable("friend");
			List<Row> rows = friendTable.getRowsWhere(new RowInfo("UUID", uuid.toString()));
			Row row = rows.get(0);
			String requests = (String) row.getColumn("REQUESTS");
			if (requests == null || requests.equalsIgnoreCase("null"))
				return 0;
			String[] size = requests.split(";");
			return size.length;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return 0;
	}

	public boolean isFriend(UUID uuid, UUID targetUUID) {
		List<String> friends = getFriends(uuid);
		if(friends == null) return false;
		if (friends.contains(NameFetcher.getName(targetUUID.toString())))
			return true;
		return false;
	}

	@SuppressWarnings("deprecation")
	public void jump(ProxiedPlayer player, ProxiedPlayer target) {
		if (isFriend(player.getUniqueId(), target.getUniqueId())) {
			player.connect(target.getServer().getInfo());
			player.sendMessage(FriendSystem.PREFIX + "�aDu wurdest zu �e" + target.getDisplayName() + " �agesendet.");
		} else
			player.sendMessage(FriendSystem.PREFIX + "�cMit diesem Spieler bist du nicht befreundet!");
	}
	
	@SuppressWarnings("deprecation")
	public void message(ProxiedPlayer player, ProxiedPlayer target, String message) {
		player.sendMessage(FriendSystem.PREFIX + "�aDu �8--> �6" + target.getDisplayName() + " �8| �e" + message);
		target.sendMessage(FriendSystem.PREFIX + "�a" + player.getDisplayName() + "�8--> �6Dir �8| �e" + message);
		lastMessage.put(player.getUniqueId(), target.getUniqueId());
		lastMessage.put(target.getUniqueId(), player.getUniqueId());
	}

	public boolean isRegistered(UUID uuid) {
		try {
			int size = database.getTable("friend").getRowsWhere("UUID", uuid.toString()).size();
			if (size >= 1)
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
