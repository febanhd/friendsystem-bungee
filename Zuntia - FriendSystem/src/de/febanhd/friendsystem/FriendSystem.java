package de.febanhd.friendsystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import de.febanhd.friendsystem.api.FriendManager;
import de.febanhd.friendsystem.commands.FriendCommand;
import de.febanhd.friendsystem.commands.MsgCommand;
import de.febanhd.friendsystem.commands.ReCommand;
import de.febanhd.friendsystem.listener.PlayerConnectionListener;
import net.Lenni0451.EasySQL.Database;
import net.Lenni0451.EasySQL.Table;
import net.Lenni0451.EasySQL.info.column.impl.BasicColumnInfo;
import net.Lenni0451.EasySQL.info.column.impl.defaults.MySQLDatabase;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

public class FriendSystem extends Plugin {

	static FriendSystem instance;
	public static FriendManager friendManager;
	public static final String PREFIX = "�7[�eFreunde�7] �r";
	
	public Database database;

	public static String db;
	public static String port;
	public static String user;
	public static String pw;
	public static String host;

	static { // Bitte nicht stehlen xD
		db = "friendsystem";
		port = "3306";
		user = "root";
		pw = "6aa5a43b3778";
		host = "127.0.0.1";
	}
	
	public void onEnable() {
		instance = this;
		if(connect()) {
			friendManager = new FriendManager();
			
			PluginManager pm = getProxy().getPluginManager();
			pm.registerListener(this, new PlayerConnectionListener());
			pm.registerCommand(this, new FriendCommand("friend"));
			pm.registerCommand(this, new MsgCommand("msg"));
			pm.registerCommand(this, new ReCommand("r"));}
	}

	public void onDisable() {
		try {
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static FriendSystem getInstance() {
		return instance;
	}

	public boolean connect() {
		getProxy().getConsole().sendMessage(new TextComponent(PREFIX + "�eVersucht eine MySQL verbindung herzustellen."));
		try {
			final String connectionString = "jdbc:mysql://" + host + ":" + port + "/" + db + "?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			Connection connection = DriverManager.getConnection(connectionString, user, pw);
			Database database = new Database(connection);
			if(!database.hasTable("friend")) {
				database.addTable("friend", new MySQLDatabase(), new BasicColumnInfo("UUID", "VARCHAR(46)", false));
				Table friend = database.getTable("friend");
				friend.addColumn(new BasicColumnInfo("NAME", "VARCHAR(46)", false));
				friend.addColumn(new BasicColumnInfo("FRIENDS", "VARCHAR(8000)", false));
				friend.addColumn(new BasicColumnInfo("REQUESTS", "VARCHAR(8000)", false));
			}
			if(!database.hasTable("players")) {
				database.addTable("players", new MySQLDatabase(), new BasicColumnInfo("UUID", "VARCHAR(46)", false));
				Table friend = database.getTable("players");
				friend.addColumn(new BasicColumnInfo("ONLINE", "VARCHAR(46)", false));
			}
			getProxy().getConsole().sendMessage(new TextComponent(PREFIX + "�aVerbindung erfolgreich aufgebaut."));
			this.database = database;
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			getProxy().getConsole().sendMessage(new TextComponent(PREFIX + "�cVerbindug konnte nicht hergestellt werden!"));
			return false;
		}
	}
	
	public Database getDatabase() {
		return database;
	}
	
	

}
