package de.febanhd.friendsystem.utils;

import java.util.List;

import de.febanhd.friendsystem.FriendSystem;
import net.Lenni0451.EasySQL.Row;
import net.Lenni0451.EasySQL.Table;
import net.Lenni0451.EasySQL.info.rows.RowInfo;

public class NameFetcher {
	public static String getName(String uuid) {
		try {
			Table friendTable = FriendSystem.getInstance().getDatabase().getTable("friend");
			List<Row> rows = friendTable.getRowsWhere(new RowInfo("UUID", uuid.toString()));
			Row row = rows.get(0);
			String name = (String) row.getColumn("NAME");	
			return name;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static String getUUID(String name) {
		try {
			Table friendTable = FriendSystem.getInstance().getDatabase().getTable("friend");
			List<Row> rows = friendTable.getRowsWhere(true, new RowInfo("NAME", name));
			Row row = rows.get(0);
			String uuid = (String) row.getColumn("UUID");
			return uuid;
		} catch (Exception e) {
			return null;
		}
	}
}
